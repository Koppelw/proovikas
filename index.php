<?php
$filePath = 'data.txt';
$delimiter = '|';
$products = array();
$i = 0;

$file = new \SplFileObject($filePath);
while (!$file->eof()) {
    $line = $file->fgetcsv($delimiter);
	$products[$i] = $line;
    $i++;
}
sort($products);

function genTree($products, $parent_node = 0, $depth=0){
$list = '<ul>';
for($i=0; $i < count($products) ; $i++){
	if($products[$i][1] == $parent_node){
		$list .= '<li>';
		$list .= $products[$i][2];
		$list .= genTree($products, $products[$i][0], $depth+1);
		$list .= '</li>';
	}
}
$list .= '</ul>';
return $list;
}

echo(genTree($products));
?>